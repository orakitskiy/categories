﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingenio.Categories
{
    public class CategoryFinder
    {
        private readonly IEnumerable<Category> _categories;

        public CategoryFinder(IEnumerable<Category> categories)
        {
            _categories = categories;
        }

        public CategoryInfo GetCategoryInfo(int categoryId)
        {
            var category = _categories.FirstOrDefault(c => c.CategoryId == categoryId);
            if (category == null)
            {
                return null;
            }

            return CategoryInfo.FromCategory(category, GetCategoryKeyWords(category));
        }

        public IEnumerable<int> GetCategoriesAtLevel(int level)
        {
            if (level < 1)
            {
                throw new ArgumentOutOfRangeException("level", "Invalid argument. Level should start from 1");
            }

            return GetCategoriesAtLevelImpl(level, _categories.Where(c => c.ParentCategoryId == -1));
        }

        private string GetCategoryKeyWords(Category category)
        {
            if (category == null)
            {
                return string.Empty;
            }

            if (string.IsNullOrEmpty(category.KeyWords))
            {
                return GetCategoryKeyWords(_categories.FirstOrDefault(c => c.CategoryId == category.ParentCategoryId));
            }

            return category.KeyWords;
        }

        private IEnumerable<int> GetCategoriesAtLevelImpl(int level, IEnumerable<Category> categories, int currentLevel = 1)
        {
            if (!categories.Any())
            {
                return Enumerable.Empty<int>();
            }

            if (level == currentLevel)
            {
                return categories.Select(c => c.CategoryId).OrderBy(c => c);
            }

            return GetCategoriesAtLevelImpl(level, _categories.Where(c => categories.Any(cc => c.ParentCategoryId == cc.CategoryId)), ++currentLevel);
        }
    }
}
