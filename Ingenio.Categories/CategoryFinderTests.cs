﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingenio.Categories
{
    using NUnit.Framework;

    [TestFixture]
    class CategoryFinderTests
    {
        private List<Category> _data = new List<Category>
        {
            new Category
            {
                CategoryId = 100,
                ParentCategoryId = -1,
                Name = "Business", 
                KeyWords = "Money"
            },
            new Category
            {
                CategoryId = 200,
                ParentCategoryId = -1,
                Name = "Tutoring",
                KeyWords = "Teaching"
            },
            new Category
            {
                CategoryId = 101,
                ParentCategoryId = 100,
                Name = "Accounting",
                KeyWords = "Taxes"
            },
            new Category
            {
                CategoryId = 102,
                ParentCategoryId = 100,
                Name = "Taxation"
            },
            new Category
            {
                CategoryId = 201,
                ParentCategoryId = 200,
                Name = "Computer"
            },
            new Category
            {
                CategoryId = 103,
                ParentCategoryId = 101,
                Name = "Corporate Tax"
            },
            new Category
            {
                CategoryId = 202,
                ParentCategoryId = 201,
                Name = "Operating System"
            },
            new Category
            {
                CategoryId = 109,
                ParentCategoryId = 101,
                Name = "Small Business Tax"
            },

        };

        private readonly CategoryFinder _finder;

        public CategoryFinderTests()
        {
            _finder = new CategoryFinder(_data);
        }

        [Test]
        public void Proper_category_is_returned_when_searched_by_id()
        {
            var info = _finder.GetCategoryInfo(100);

            Assert.AreEqual(100, info.CategoryId);
            Assert.AreEqual(-1, info.ParentCategoryId);
            Assert.AreEqual("Business", info.Name);
            Assert.AreEqual("Money", info.KeyWords);
        }

        [Test]
        public void Category_Keywords_is_taken_from_parent_if_empty()
        {
            var info = _finder.GetCategoryInfo(102);

            Assert.AreEqual(102, info.CategoryId);
            Assert.AreEqual(100, info.ParentCategoryId);
            Assert.AreEqual("Taxation", info.Name);
            Assert.AreEqual("Money", info.KeyWords);
        }

        [Test]
        public void Category_Keywords_is_taken_from_the_parent_where_they_first_defined()
        {
            var info = _finder.GetCategoryInfo(202);

            Assert.AreEqual(202, info.CategoryId);
            Assert.AreEqual(201, info.ParentCategoryId);
            Assert.AreEqual("Operating System", info.Name);
            Assert.AreEqual("Teaching", info.KeyWords);
        }

        [Test]
        public void Only_root_categories_are_returned_when_first_level_is_queried()
        {
            var categories = _finder.GetCategoriesAtLevel(1);
            CollectionAssert.AreEqual(categories, new[] {100, 200});
        }

        [Test]
        public void Proper_categories_returned_for_existing_level()
        {
            var categories = _finder.GetCategoriesAtLevel(2);
            CollectionAssert.AreEqual(categories, new[] { 101, 102, 201 });

            categories = _finder.GetCategoriesAtLevel(3);
            CollectionAssert.AreEqual(categories, new[] { 103, 109, 202 });
        }

        [Test]
        public void Empty_enumerable_returned_when_there_are_no_categories_at_level_n()
        {
            var categories = _finder.GetCategoriesAtLevel(100);
            CollectionAssert.AreEqual(categories, Enumerable.Empty<int>());
        }

        [Test]
        public void InvalidArgumentException_is_raised_when_incorrect_level_is_passed()
        {
            Assert.Throws(typeof(ArgumentOutOfRangeException), () => _finder.GetCategoriesAtLevel(0));
        }
    }
}
