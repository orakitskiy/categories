﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingenio.Categories
{
    public class Category
    {
        public int CategoryId { get; set; }

        public int ParentCategoryId { get; set; }

        public string Name { get; set; }

        public string KeyWords { get; set; }
    }

    public class CategoryInfo : Category
    {
        public static CategoryInfo FromCategory(Category category, string keyWords)
        {
            var info = new CategoryInfo
            {
                CategoryId = category.CategoryId,
                ParentCategoryId = category.ParentCategoryId,
                Name = category.Name,
                KeyWords = keyWords
            };

            return info;
        }
    }
}
